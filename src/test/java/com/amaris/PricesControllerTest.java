package com.amaris;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.amaris.model.Price;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class PricesControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/api/home")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello World")));
	}
	
	/**
	 * Test Case for the condition: Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
	 * @throws Exception
	 */
	@Test
	public void getPriceByDateProductBrandTest1() throws Exception {
		
		String target_date = "2020-06-14 10:00:00";
		String brandId = "1";
		String productId = "35455";
		
		ResultActions resultActions= this.mockMvc.perform(get("/api/priceproduct")
				.param("target_date", target_date)
				.param("brandId", brandId)
				.param("productId", productId)
				).andExpect(status().isOk());
		
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		
		ObjectMapper m = new ObjectMapper();
		
		Price price = m.readValue(contentAsString, new TypeReference<Price>() {});
		
		if(price != null) {
			
				System.out.println(price.toString());	
		}
		
		assertFalse(price == null);
				
	}	
	
	/**
	 * Test Case for the condition: Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
	 * @throws Exception
	 */
	@Test
	public void getPriceByDateProductBrandTest2() throws Exception {
		
		String target_date = "2020-06-14 16:00:00";
		String brandId = "1";
		String productId = "35455";
		
		ResultActions resultActions= this.mockMvc.perform(get("/api/priceproduct")
				.param("target_date", target_date)
				.param("brandId", brandId)
				.param("productId", productId)
				).andExpect(status().isOk());
		
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		
		ObjectMapper m = new ObjectMapper();
		
		Price price = m.readValue(contentAsString, new TypeReference<Price>() {});
		
		if(price != null) {
			
				System.out.println(price.toString());	
		}
		
		assertFalse(price == null);
				
				
	}	
	
	/**
	 * Test Case for the condition: Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)
	 * @throws Exception
	 */
	@Test
	public void getPriceByDateProductBrandTest3() throws Exception {
		
		String target_date = "2020-06-14 21:00:00";
		String brandId = "1";
		String productId = "35455";
		
		ResultActions resultActions= this.mockMvc.perform(get("/api/priceproduct")
				.param("target_date", target_date)
				.param("brandId", brandId)
				.param("productId", productId)
				).andExpect(status().isOk());
		
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		
		ObjectMapper m = new ObjectMapper();
		
		Price price = m.readValue(contentAsString, new TypeReference<Price>() {});
		
		if(price != null) {
			
				System.out.println(price.toString());	
		}
		
		assertFalse(price == null);
				
				
	}		
	
	/**
	 * Test Case for the condition: Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)
	 * @throws Exception
	 */
	@Test
	public void getPriceByDateProductBrandTest4() throws Exception {
		
		String target_date = "2020-06-15 10:00:00";
		String brandId = "1";
		String productId = "35455";
		
		ResultActions resultActions= this.mockMvc.perform(get("/api/priceproduct")
				.param("target_date", target_date)
				.param("brandId", brandId)
				.param("productId", productId)
				).andExpect(status().isOk());
		
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		
		ObjectMapper m = new ObjectMapper();
		
		Price price = m.readValue(contentAsString, new TypeReference<Price>() {});
		
		if(price != null) {
			
				System.out.println(price.toString());	
		}
		
		assertFalse(price == null);
							
	}	
	
	/**
	 * Test Case for the condition: Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)
	 * @throws Exception
	 */
	@Test
	public void getPriceByDateProductBrandTest5() throws Exception {
		
		String target_date = "2020-06-16 21:00:00";
		String brandId = "1";
		String productId = "35455";
		
		ResultActions resultActions= this.mockMvc.perform(get("/api/priceproduct")
				.param("target_date", target_date)
				.param("brandId", brandId)
				.param("productId", productId)
				).andExpect(status().isOk());
		
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		
		ObjectMapper m = new ObjectMapper();
		
		Price price = m.readValue(contentAsString, new TypeReference<Price>() {});
		
		if(price != null) {
			
				System.out.println(price.toString());	
		}
		
		assertFalse(price == null);
				
	}	
}

package com.amaris.interfaces;

import java.util.List;

import com.amaris.model.Price;

/**
 * Class that define an interface with the methosds that must implement the PricesService
 * @author Fernando Olivares
 *
 */
public interface PricesServiceInterface {

	/**
	 * Method that return the prices stored in the database. According to a input parameters conditions. 
	 * @param target_date: Date that are between start_date and end_date for each price record
	 * @param brandId: value to compare with the values into database
	 * @param productId: value to compare with the values into database
	 * @return Return as output data: and Price object that complies with the input conditions
	 */
	public Price getPriceByBrandIdAndProductId(String target_date, String strProductId,  String strBrandId);

}

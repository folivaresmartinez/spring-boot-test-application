package com.amaris.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.Utils.Consts;
import com.amaris.Utils.Functions;
import com.amaris.interfaces.PricesServiceInterface;
import com.amaris.model.Price;
import com.amaris.repository.PricesRepository;

/**
 * Class that define the business logic
 * @author Fernando Olivares
 *
 */
@Service
public class PricesService implements PricesServiceInterface
{
   /**
    * Prices Repository object
    */
	@Autowired
	private PricesRepository pricesRepository;
	//getting all books record by using the method findaAll() of CrudRepository
		
	/**
	 * Default Contructor
	 */
	@Autowired
	public PricesService() { }	
	

	/**
	 * Method that return the prices stored in the database. According to a input parameters conditions. 
	 * @param target_date: Date that are between start_date and end_date for each price record
	 * @param brandId: value to compare with the values into database
	 * @param productId: value to compare with the values into database
	 * @return Return as output data: and Price object that complies with the input conditions
	 */
	@Override
	public Price getPriceByBrandIdAndProductId(String target_date, String strBrandId, String  strProductId) {
		
		Price priceToReturn = null;
		
		List<Price> prices = new ArrayList<Price>();		
		List<Price> pricesFiltered = new ArrayList<Price>();
		
		int brandId = Functions.ConvertFromStringToInt(strBrandId);
		int productId = Functions.ConvertFromStringToInt(strProductId);		
		Date dateQuery = Functions.ParseDateValid(target_date);
		
		if(brandId != Consts.UNDEFINED && productId != Consts.UNDEFINED) {		
			pricesRepository.findByBrandIdAndProductId(brandId, productId).forEach(prices::add);
			
			pricesFiltered = pricesRepository.findByBrandIdAndProductId(brandId, productId).stream().filter(oPrice ->
						(oPrice.getStartDate().before(dateQuery) && 
						 oPrice.getEndDate().after(dateQuery))).sorted(Comparator.comparingInt(Price::getPriority).reversed())
			  .collect(Collectors.toList());						
		}	
		
		if(pricesFiltered != null && pricesFiltered.size() >= 1) {
			priceToReturn = pricesFiltered.get(0);
		}
		
		return priceToReturn;
	}
}
package com.amaris.Utils;

/**
 * Class to define the constants used in the microservice
 * @author Fernando Olivares
 *
 */
public final class Consts {

   
	/** UNDEFINED Const  */
	public final static int UNDEFINED = -1;    
	
	/** DATE FORMAT used to convert string dates to Date objects */
	public final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	  // PRIVATE //

   	/**
   	The caller references the constants using <tt>Consts.EMPTY_STRING</tt>, 
   	and so on. Thus, the caller should be prevented from constructing objects of 
   	this class, by declaring this private constructor. 
	*/
	
	/**
	 * This prevents even the native class from calling this ctor as well :
	 */
	private Consts(){

		throw new AssertionError();
	}	
    
}
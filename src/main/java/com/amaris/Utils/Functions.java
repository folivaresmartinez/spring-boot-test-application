package com.amaris.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.LogFactory;

/**
 * Class that implement aux. functions used into the microservice
 * @author Fernando Olivares
 *
 */
public class Functions {
	
	/**
	 * Function to convert an string input to an integer value
	 * @param valueToConvert
	 * @return: Integer value converted
	 */
	public static int ConvertFromStringToInt(String valueToConvert) {
	
	    int valueReturn = Consts.UNDEFINED;
    
    
		try{
	    	
			valueReturn = Integer.parseInt(valueToConvert);
	    }
	    catch (NumberFormatException ex){
	    	LogFactory.getLog(Functions.class).error("Error in call to ConvertFromStringToInt, Value: " + valueToConvert , ex);
	    }
		
		return valueReturn;	
    }
	
	
	/**
	 * Function to convert an input string parameter to a valid Date object
	 * @param strDate
	 * @return A valid Date
	 */
	public static Date ParseDateValid(String strDate) 
	{
        Date dateReturn = null;
        
		try {
        
			if(strDate != null) {
				
				SimpleDateFormat dateFormat = new SimpleDateFormat(Consts.DATE_FORMAT) ;
				dateReturn = dateFormat.parse(strDate);
				
				System.out.println("Date: " + dateReturn.toString());
			}
            
        } catch (java.text.ParseException ex) {
        	LogFactory.getLog(Functions.class).error("Error in call to ParseDateValid, Value: " + strDate , ex);
		}
		
		return dateReturn;
	}
}

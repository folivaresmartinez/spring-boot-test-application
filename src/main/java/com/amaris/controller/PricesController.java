package com.amaris.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.model.Price;
import com.amaris.service.PricesService;

/**
 * Class that implement the Rest Api exposed by the SpringBootTestApplication
 * @author Fernando Olivares
 *
 */
@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class PricesController {
	
	/**
	 * Logger of the aplication
	 */
	private static final Logger logger = LoggerFactory.getLogger(PricesController.class);
	
	/**
	 * Autowire the PricesService class
	 */
	@Autowired
	PricesService pricesService;

	/**
	 * Test method published by the REST controller
	 * @return
	 */
	@GetMapping("/home")
	public String Home() {
		return "Hello World!";
	}	
		
	/**
	 * Method that return the prices stored in the database. According to a input parameters conditions. 
	 * @param target_date: Date that are between start_date and end_date for each price record
	 * @param brandId: value to compare with the values into database
	 * @param productId: value to compare with the values into database
	 * @return Return as output data: and Price object that complies with the input conditions
	 */
	@GetMapping("/priceproduct")
	public ResponseEntity<Price> getPriceByDateProductBrand(@RequestParam(required=true) String target_date, 
													@RequestParam(required=true) String brandId,
													@RequestParam(required=true) String productId) {
		try {
			
			Price price = null;
			
			//We check the input parameters			
			if(target_date == null || productId == null || brandId == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
			price = pricesService.getPriceByBrandIdAndProductId(target_date, brandId, productId);


			if (price == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(price, HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.amaris.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * Class that implement an POJO object that represent the prices objects stored into the database
 * 
 * @author Fernando Olivares
 *
 */
@Getter
@Setter
@Entity
@Table(name = "prices")
public class Price {

	/**
	 * ID field of the price record.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	/**
	 * brand_id field of the price record.
	 */
	@Column(name = "brand_id")
	private int brandId;

	/**
	 * start_date field of the price record.
	 */	@Column(name = "start_date")
	private Date  startDate;
	
	/**
	 * end_date field of the price record.
	 */
	@Column(name = "end_date")
	private Date endDate;
	
	/**
	 * price_list field of the price record.
	 */
	@Column(name = "price_list")
	private int priceList;
	
	/**
	 * product_id field of the price record.
	 */
	@Column(name = "product_id")
	private int productId;
	
	/**
	 * priority field of the price record.
	 */
	@Column(name = "priority")
	private int priority;
	
	/**
	 * price field of the price record.
	 */
	@Column(name = "price")
	private double price;
	
	/**
	 * curr field of the price record.
	 */
	@Column(name = "curr")
	private String curr;

	
	/**
	 * Default constructor of the class
	 */
	public Price() {

	}

	/**
	 * Constructor with parameters.
	 */
	public Price(int brand_id, Date  start_date, Date end_date, int price_list, int product_id, int priority, double d, String curr) {
		
		this.brandId = brand_id;
		this.startDate = start_date;
		this.endDate = end_date;
		this.priceList = price_list;
		this.productId = product_id;
		this.priority = priority;
		this.price = d;
		this.curr = curr;
	}


	/**
	 * Aux function to print a price object
	 */
	@Override
	public String toString() {
		return "Price: "				+
				"\n[" 					+
				"\nBrand_Id:		" 	+ this.brandId + 
				"\nStart_Date:		" 	+ this.startDate + 
				"\nEnd_Date:		" 	+ this.endDate +
				"\nPrice_List:		" 	+ this.priceList +
				"\nProduct_Id:		" 	+ this.productId +
				"\nPriority:		" 	+ this.priority +
				"\nPrice:			" 	+ this.price +
				"\nCurr:			" 	+ this.curr + 
				"\n]";
	}

}

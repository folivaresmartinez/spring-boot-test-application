package com.amaris.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.amaris.model.Price;

/**
 * Interface that implement the PricesRepository with the functions used into the Prices Service
 * @author Fernando Olivares
 *
 */
@Component
public interface PricesRepository extends JpaRepository<Price, Long> {
 
	/**
	 * Method that return the prices stored in the database. According to a input parameters conditions. 
	 * @param brandId: value to compare with the values into database
	 * @param productId: value to compare with the values into database
	 * @return Return as output data: and Price object that complies with the input conditions
	 */  
	List<Price> findByBrandIdAndProductId(int brand_id, int priceId);
  
  
}

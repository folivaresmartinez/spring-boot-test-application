package com.amaris;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Main class for the SpringBootTestApplication
 * @author Fernando Olivares
 * @version 1.0
 *
 */
@SpringBootApplication
public class SpringBootTestApplication 
{
	public static void main(String[] args) 
	{
		SpringApplication.run(SpringBootTestApplication.class, args);
	}
}

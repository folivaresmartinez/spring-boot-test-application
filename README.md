# spring-boot-test-application
Amaris Test Application

# Port where the microservice publish the API Methods
Microservice Port: 8080

# Compile and run the microservice
mvn clean install

mvn spring-boot:run

# Run Test
mvn test


